# Assignment 2 - Agile Software Practice.

Name: Chengfeng Pan  

## Client UI.


![][1]

>>allows the user add a new project    
>>delete a project  
>>edit a project  
>>display a project  


![][2]

>>allows the user to select a project  
>>add a new stage  
>>delete a stage    

![][3]

>>allows the user to edit a stage  

![][4]
>>allows the user to edit a task name, status, task content  

![][5]
>>allows the user to display team members  

![][6]
>>allows the user to edit a team  


![][8]
>>allows the user to display task  


![][9]
>>allows the user to display staff   


![][10]
>>allows the user to add a project


## E2E/Cypress testing.  
### Cypress Test Execution
https://dashboard.cypress.io/projects/xx3zur/runs

## Web API CI.

coverage report  
https://gitlab.com/pcf1998/tracings-cicd/blob/master/README.md

Web API GitLab repo:  
https://gitlab.com/pcf1998/tracings-cicd  

Web API Staging URL:  
https://tracings-staging-api.herokuapp.com/  

Web API Production URL:  
https://tracings-prod-api.herokuapp.com/  

## Client Vue CI.  
Client GitLab repo:  
https://gitlab.com/pcf1998/tracingsvue-cicd  

Client Staging URL:  
http://tracings-staging.surge.sh/  

Client Production URL:  
http://tracings-production.surge.sh/

[1]: ./img/projects.png  
[2]: ./img/displayproject1.png  
[3]: ./img/displayproject2.png 
[4]: ./img/displayproject3.png
[5]: ./img/displayproject4.png 
[6]: ./img/displayteam1.png
[7]: ./img/displayteam2.png 
[8]: ./img/tasks.png 
[9]: ./img/staff.png  
[10]: ./img/addproject.png
