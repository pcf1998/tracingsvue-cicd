describe('Edit project page', () => {

    beforeEach(() => {
        cy.visit('/')
        // Click Manage Projects navbar link
        cy.get('.navbar-nav')
            .eq(0)
            .find('.nav-item')
            .eq(1)
            .click()
    })

    describe('For a adding project operation', () => {
        it('should add a project', () => {

            cy.get('i')
                .eq(9)
                .click()
            cy.url().should('include', '/project')

            cy.get('button')
                .eq(1)
                .click()
            cy.get('.typo__p')
                .should('contain.html', 'Project Added Successfully!')

        })

    })

})
