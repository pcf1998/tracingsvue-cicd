describe('Edit project page', () => {
    beforeEach(() => {
        cy.visit('/')
        // Click Manage Projects navbar link
        cy.get('.navbar-nav')
            .eq(0)
            .find('.nav-item')
            .eq(1)
            .click()
    })

    describe('For a edit project operation', () => {
        it('should edit the selected project', () => {
            cy.get('tbody')
                .find('tr')
                .should('have.length', 25)
            cy.get('thead')
                .find('tr')
                .eq(0)
                .find('th')
                .eq(1)
                .click()
            cy.get('tbody')
                .find('tr')
                .eq(0)
                .find('td')
                .eq(9)
                .find('a')
                .click()
            cy.url().should('include', '/editproject')

            cy.get('select')
                .select('processing')
            cy.get('button')
                .eq(1)
                .click()
            cy.get('.typo__p')
                .should('contain.html', 'Project Added Successfully!')
        })

    })

})
