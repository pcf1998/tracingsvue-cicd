describe('Home page', function() {

    beforeEach(() => {
        cy.visit("/");
    });

    it("load home page", () => {
        cy.visit('http://localhost:8080/')
    });


    it("Shows a header", () => {
        cy.get(".vue-title").should("contain", "Time for working !!!");
    });

    describe("Navigation bar", () => {

        it("Shows the required links", () => {
            cy.get(".navbar-nav")
                .eq(0)
                .within(() => {
                    cy.get(".nav-item")
                        .eq(0)
                        .should("contain", "Home");
                    cy.get(".nav-item")
                        .eq(1)
                        .should("contain", "Projects");
                    cy.get(".nav-item")
                        .eq(2)
                        .should("contain", "Teams");
                    cy.get(".nav-item")
                        .eq(3)
                        .should("contain", "Tasks");
                    cy.get(".nav-item")
                        .eq(4)
                        .should("contain", "Staff");
                });
            cy.get(".navbar-nav")
                .eq(1)
                .within(() => {
                    cy.get(".nav-item")
                        .eq(0)
                        .should("contain", "Login");
                    cy.get(".nav-item")
                        .eq(1)
                        .should("contain", "Logout");
                });
        });


        it("Redirects when links are clicked", () => {
            cy.get("[data-test=projectsbtn]").click();
            cy.url().should("include", "/projects");
            cy.get(".navbar-nav")
                .eq(0)
                .find(".nav-item")
                .eq(2)
                .click();
            cy.url().should("include", "/teams");
            cy.get(".navbar-nav")
                .eq(0)
                .find(".nav-item")
                .eq(3)
                .click();
            cy.url().should("include", "/tasks");
            cy.get(".navbar-nav")
                .eq(0)
                .find(".nav-item")
                .eq(4)
                .click();
            cy.url().should("include", "/users");
        });

    })

    describe("Page text", () => {

        it("Shows the links in text", () => {
            cy.get("p")
                .eq(1)
                .find('a')
                .click();
            cy.url().should("include", "/projects");


        });

    })
})
