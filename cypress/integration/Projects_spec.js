describe("Manage projects page", () => {
    beforeEach(() => {
        cy.visit("/");
        // Click Manage Projects navbar link
        cy.get(".navbar-nav")
            .eq(0)
            .find(".nav-item")
            .eq(1)
            .click();
    });

    describe("For a canceled delete operation", () => {
        it("reduces the no. of projects by 1", () => {
            cy.get("tbody")
                .find("tr")
                .should("have.length", 25);
            cy.get("tbody")
                .find("tr")
                .eq(2)
                .find("td")
                .eq(10)
                .find("a")
                .click()
            cy.get("button")
                .contains("Cancel")
                .click()
            cy.get("button")
                .contains("OK")
                .click()
            cy.get("tbody")
                .find("tr")
                .should("have.length", 25);
        });
    });

})
