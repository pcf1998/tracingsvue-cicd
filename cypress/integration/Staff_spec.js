describe('Manage staff page', () => {

    beforeEach(() => {
        cy.visit('/')
        // Click Manage Projects navbar link
        cy.get('.navbar-nav')
            .eq(0)
            .find('.nav-item')
            .eq(4)
            .click()
    })

    describe('For list staff', () => {
        it('the number of staff should be 25', () => {
            cy.get("tbody")
                .find("tr")
                .should("have.length", 25);
        })

        it('the number of staff should be 10', () => {
            cy.get("select[name=limit]")
                .select("10")
            cy.get("tbody")
                .find("tr")
                .should("have.length", 10);
        })

        it('the number of staff should be 10 TOO', () => {
            cy.get("select[name=limit]")
                .select("10")
            cy.get("tbody")
                .find("tr")
                .should("have.length", 10);
            cy.get('li')
                .eq(10)
                .click()
            cy.get("tbody")
                .find("tr")
                .should("have.length", 10);

        })

    })
})
