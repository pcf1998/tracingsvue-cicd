describe('Manage tasks page', () => {

    beforeEach(() => {
        cy.visit('/')
        // Click Manage Projects navbar link
        cy.get('.navbar-nav')
            .eq(0)
            .find('.nav-item')
            .eq(3)
            .click()
    })

    describe('For show task', () => {
        it('should display the task id', () => {
            cy.get('select')
                .eq(0)
                .select('test project name')
            cy.get('select')
                .eq(1)
                .select('team000-test')
            cy.get('select')
                .eq(2)
                .select('task0009 -test')
            cy.get('p')
                .eq(8)
                .should('contain','Task ID:')
        })

        it('should display the task name', () => {
            cy.get('select')
                .eq(0)
                .select('test project name')
            cy.get('select')
                .eq(1)
                .select('team000-test')
            cy.get('select')
                .eq(2)
                .select('task0009 -test')
            cy.get('p')
                .eq(9)
                .should('contain','Task Name:')
        })

        it('should display the task status', () => {
            cy.get('select')
                .eq(0)
                .select('test project name')
            cy.get('select')
                .eq(1)
                .select('team000-test')
            cy.get('select')
                .eq(2)
                .select('task0009 -test')
            cy.get('p')
                .eq(10)
                .should('contain','Status:')
        })

        it('should display the task content', () => {
            cy.get('select')
                .eq(0)
                .select('test project name')
            cy.get('select')
                .eq(1)
                .select('team000-test')
            cy.get('select')
                .eq(2)
                .select('task0009 -test')
            cy.get('p')
                .eq(11)
                .should('contain','Task Content:')
        })

        it('should display the last modified time', () => {
            cy.get('select')
                .eq(0)
                .select('test project name')
            cy.get('select')
                .eq(1)
                .select('team000-test')
            cy.get('select')
                .eq(2)
                .select('task0009 -test')
            cy.get('p')
                .eq(12)
                .should('contain','Last Modified Time:')
        })

        it('should display the created time', () => {
            cy.get('select')
                .eq(0)
                .select('test project name')
            cy.get('select')
                .eq(1)
                .select('team000-test')
            cy.get('select')
                .eq(2)
                .select('task0009 -test')
            cy.get('p')
                .eq(13)
                .should('contain','Created Time:')
        })
    })





})
