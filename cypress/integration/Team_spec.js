describe('Display team page', () => {

    it('should go to the display team page', () => {
        cy.visit('/')
        cy.get('.navbar-nav')
            .eq(0)
            .find('.nav-item')
            .eq(2)
            .click()
        cy.get('select')
            .select('test project name')
        cy.get('tbody')
            .find('tr')
            .should('have.length', 2)
        cy.get('thead')
            .find('tr')
            .eq(0)
            .get('th')
            .eq(1)
            .click()
        cy.get('tbody')
            .find('tr')
            .eq(0)
            .get('td')
            .eq(0)
            .find('a')
            .click()
        cy.url().should('include', '/displayteam')
    })

    it('should edit team name', () => {
        cy.get('i')
            .eq(9)
            .click()
        cy.get('#editteamnamebtnsubmit').should('have.id','editteamnamebtnsubmit')

    })

    it('should cancel edit team name', () => {
        cy.get('#editteamnamebtncancel').should('have.id','editteamnamebtncancel')
    })

    it('should edit team status', () => {
        cy.get('#editteamnamebtncancel')
            .click()
        cy.get('i')
            .eq(10)
            .click()
        cy.get('#editteamstatusbtnsubmit').should('have.id','editteamstatusbtnsubmit')

    })

    it('should cancel edit team status', () => {
        cy.get('#editteamstatusbtncancel')
            .click()
        cy.get('#editteamstatusbtncancel')
            .should('not.have.id','editteamstatusbtncancel')

    })

    it('should add a team member', () => {
        cy.get('i')
            .eq(12)
            .click()
        cy.get('#addteammemberbtnsubmit')
            .should('have.id','addteammemberbtnsubmit')
    })

    it('should cancel add a team member', () => {
        cy.get('#addteammemberbtncancel')
            .click()
        cy.get('#addteammemberbtncancel')
            .should('not.have.id','addteammemberbtncancel')
    })

})
