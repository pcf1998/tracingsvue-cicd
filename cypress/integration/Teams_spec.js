describe('Manage teams page', () => {

    beforeEach(() => {
        cy.visit('/')
        // Click Manage Projects navbar link
        cy.get('.navbar-nav')
            .eq(0)
            .find('.nav-item')
            .eq(2)
            .click()
    })

    describe('For list teams', () => {
        it('the number of should be 0', () => {
            cy.get('select')
                .select('2019-10-01')
            cy.get("tbody")
                .find("tr")
                .should("have.length", 1);

        })
    })

    describe('For delete a team', () => {
        it('cancel delete a team', () => {
            cy.get('select')
                .select('test project name')
            cy.get("tbody")
                .find("tr")
                .should("have.length", 2);

            cy.get("tbody")
                .find("tr")
                .eq(0)
                .find("td")
                .eq(8)
                .find("a")
                .click()
            cy.get("button")
                .contains("Cancel")
                .click()
            cy.get("button")
                .contains("OK")
                .click()
            cy.get("tbody")
                .find("tr")
                .should("have.length", 2);

        })
    })
})
