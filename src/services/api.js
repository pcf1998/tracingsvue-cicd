import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: 'https://tracings-prod-api.herokuapp.com/'
  })
}
